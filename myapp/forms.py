from django import forms

class KegiatanForm(forms.Form):
	kegiatan = forms.CharField(
		max_length=30)

	tanggal = forms.DateField(
		widget=forms.DateInput(attrs={'type': 'date'}))

	jam = forms.TimeField(
		widget=forms.TimeInput(attrs={'type': 'time'}))

	tempat = forms.CharField(
		max_length=20)

	kategori = forms.CharField(
		max_length=20)