from django.urls import path
from . import views

urlpatterns = [
    path('', views.beranda, name='beranda'),
    path('tentang_saya', views.tentang_saya, name='tentang_saya'),
    path('galeri_kucing', views.galeri_kucing, name='galeri_kucing'),
    path('jadwal', views.jadwal, name='jadwal'),
    path('delete-jadwal', views.delete_jadwal, name='delete_jadwal'),
]