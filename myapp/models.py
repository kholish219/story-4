from django.db import models

# Create your models here.
class Jadwal(models.Model):
	# hari, tanggal, jam, nama kegiatan, tempat, dan kategori
	kegiatan = models.CharField(max_length=30)
	tanggal = models.DateField()
	jam = models.TimeField()
	tempat = models.CharField(max_length=20)
	kategori = models.CharField(max_length=20)
	
	def __str__(self):
		return self.kegiatan