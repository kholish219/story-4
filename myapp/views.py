from django.shortcuts import render, redirect
from .forms import KegiatanForm
from .models import Jadwal

DAYS = [
	'Senin',
	'Selasa',
	'Rabu',
	'Kamis',	
	"Jum'at",
	'Sabtu',
	'Minggu',
]

# Create your views here.
def beranda(request):
	return render(request, 'index.html', {'title': 'Beranda'})

def tentang_saya(request):
	return render(request, 'tentang_saya.html', {'title': 'Tentang Saya'})

def galeri_kucing(request):
	return render(request, 'page_tambahan.html', {'title': 'Galeri Kucing'})

def jadwal(request):
	if request.method == 'POST':
		form = KegiatanForm(request.POST)
		if form.is_valid():
			input_jadwal=Jadwal(kegiatan = form.data['kegiatan'],
				tanggal = form.data['tanggal'],
				jam = form.data['jam'],
				tempat = form.data['tempat'],
				kategori = form.data['kategori'],)
			input_jadwal.save()
			return redirect('jadwal') 
	else:
		form = KegiatanForm()

	temp = Jadwal.objects.all()

	for jadwal in temp:
		jadwal.hari = DAYS[jadwal.tanggal.weekday()]

	return render(request, 'jadwal.html', {'title': 'Jadwal', 'form_ini': form, 'isi_jadwal': temp })

def delete_jadwal(request):
	if request.method == 'POST':
		id_jadwal = request.POST['delete']
		Jadwal.objects.get(id=id_jadwal).delete()
	return redirect('jadwal')